#!/bin/bash
#Note: 
# * Comprobe 1 argument of input
# * Create function 
# * Create Usage
VM_NAME="${1}"
PATH_VMFS="/vmfs/volumes/sa-esxi-01-01"
PATH_VM="${PATH_VMFS}/${VM_NAME}"

echo -e "\nStep 1. Clone the VMDK to a thin provisioned disk using vmkfstools"
/bin/vmkfstools -i "${PATH_VM}/${VM_NAME}.vmdk" -d thin "${PATH_VM}/${VM_NAME}-thin.vmdk"

echo -e "\nStep 2. Rename old flat file"
mv "${PATH_VM}/${VM_NAME}-flat.vmdk" "${PATH_VM}/${VM_NAME}-flat.vmdk.old"

echo -e "\nStep 3. Rename new flat file"
mv "${PATH_VM}/${VM_NAME}-thin-flat.vmdk" "${PATH_VM}/${VM_NAME}-flat.vmdk"

echo -e "Step 4. Unregister VM from ESXi
  This is necessary for the host to recognize the new disk type. 'Right-click VM' > 'Unregister'"
read -p "Press [Enter] to continue..."

echo -e "\nStep 5. Register VM in ESXi
  Click on Create / Register VM.
  Select Register an existing virtual machine. Click on Next"
read -p "Press [Enter] to continue..."

echo -e "\nStep 6. Verify hard disk type conversion \n
  'Right-click' on the 'VM' and select Edit Settings \n
  Expand hard disk and check the Type. It shows as 'Thin provisioned'"
read -p "Press [Enter] to continue..."

echo -e "\nStep 7. Start the VM \
  Start the VM and check that everything works as expected"
read -p "Press [Enter] to continue..."

echo -e "\nStep 8. Delete unnecessary VMDK files"
rm -f "${PATH_VM}/${VM_NAME}-thin.vmdk" "${PATH_VM}/${VM_NAME}-flat.vmdk.old"
