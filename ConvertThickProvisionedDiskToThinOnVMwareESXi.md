## Convert Thick Provisioned Disk To Thin On VMware ESXi

[Convert thick provisioned disk to thin on VMware ESXi](https://www.alitajran.com/convert-thick-provisioned-disk-to-thin-on-vmware-esxi/)



### Step 1. Navigate to the VM folder
Navigate to the VM folder under /vmfs/volumes/ with the command cd vmfs/volumes.

```sh
[root@localhost:~] cd vmfs/volumes
[root@localhost:/vmfs/volumes] cd dataStore
[root@localhost:/vmfs/volumes/5e88aaf7-7ab335e2-4adc-1c697a0f86bd]
```



### Step 2. Clone the VMDK to a thin provisioned disk using vmkfstools

Use the command 
```sh
vmkfstools -i FS01-2016.vmdk -d thin FS01-2016-thin.vmdk
```

### Step 3. Rename old flat file

Rename the old file with the command mv FS01-2016-flat.vmdk FS01-2016-flat.vmdk.old.
```sh
[root@localhost:/vmfs/volumes/5e88aaf7-7ab335e2-4adc-1c697a0f86bd/FS01-2016] mv FS01-2016-flat.vmdk FS01-2016-flat.vmdk.old
```


### Step 4. Rename new flat file
Rename the thin flat file with the command mv FS01-2016-thin-flat.vmdk FS01-2016-flat.vmdk.
```sh
[root@localhost:/vmfs/volumes/5e88aaf7-7ab335e2-4adc-1c697a0f86bd/FS01-2016] mv FS01-2016-thin-flat.vmdk FS01-2016-flat.vmdk
```


### Step 5. Unregister VM from ESXi

This is necessary for the host to recognize the new disk type. "Right-click VM" > "Unregister"


### Step 6. Register VM in ESXi

Click on **Create** / **Register VM**.


Select **Register an existing virtual machine**. Click on **Next**


### Step 7. Verify hard disk type conversion


"Right-click" on the "VM" and select **Edit Settings**

Expand hard disk and check the **Type**. It shows as "Thin provisioned"


### Step 8. Start the VM

Start the VM and check that everything works as expected.


### Step 9. Delete unnecessary VMDK files

Clean up and remove the files. Run the command **rm FS01-2016-thin.vmdk && rm FS01-2016-flat.vmdk.old**

```sh
[root@localhost:/vmfs/volumes/5e88aaf7-7ab335e2-4adc-1c697a0f86bd/FS01-2016] rm FS01-2016-thin.vmdk && rm FS01-2016-flat.vmdk.old
```
